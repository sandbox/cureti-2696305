<?php

/**
 * @file
 * Administration functions for the Cureti Phone Login module.
 */

/**
 * Menu callback: admin configure options for cureti.
 */
function cureti_phone_login_admin_form() {
  $form = array();
  $form['cureti_phone_login_merchant_id'] = array(
    '#type' => 'textfield',
    '#required' => TRUE,
    '#title' => t('Merchant Secret'),
    '#default_value' => variable_get('cureti_phone_login_merchant_id') ? variable_get('cureti_phone_login_merchant_id') : variable_get('cureti_phone_login_merchant_id'),
    '#attributes' => array('placeholder' => t('Your Merchant Id')),
  );

  $cureti_api_key = variable_get('cureti_phone_login_api_key');
  $form['cureti_phone_login_api_key'] = array(
    '#type' => 'textfield',
    '#required' => TRUE,
    '#title' => t('API Key'),
    '#default_value' => $cureti_api_key ? $cureti_api_key : '',
    '#attributes' => array('placeholder' => t('Your API Key')),
  );

  $allow_password_set = variable_get('cureti_phone_login_password_change');
  $form['cureti_phone_login_password_change'] = array(
    '#type' => 'checkbox',
    '#default_value' => $allow_password_set ? $allow_password_set : '',
    '#title' => t('Allow users to set their password after login if the password is not set.'),
  );

  return system_settings_form($form);
}
