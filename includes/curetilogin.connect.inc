<?php
/**
 * @file
 * Connect file for Cureti One Click Login Integration.
 *
 * This file handles the connect action when the user is returned
 * from the Cureti Phone Login API.
 */

/**
 * Connect action page while returning from Cureti Login API.
 *
 * @return string
 *   Error message if any.
 */
function cureti_phone_login_action_page() {
  $error_message = t('The Cureti Phone login could not be completed due to an error. Please create an account or contact us directly. Details about this error have already been recorded to the error log.');
  if (isset($_GET['token']) && $_GET['token']) {
    $access_token = $_GET['token'];
    $details = cureti_phone_login_access_token($access_token);
    if ($details && isset($details->phone) && $details->phone && isset($details->status) && $details->status) {
      // Removing the + sign from the phone number.
      $phone = ltrim($details->phone, "+");
      // Checking if the user is already exists.
      $user = user_load_by_name($phone);
      if (!$user) {
        // If the user does not exist, we register the new user.
        if (variable_get('user_register', 1)) {
          $user = cureti_phone_login_create_user($phone);
          if (!isset($user) || empty($user)) {
            drupal_set_message(t('Unable to create a new account using Cureti Phone Login.'), 'warning');
            return FALSE;
          }
          else {
            drupal_set_message(t('Your account is successfully created'));
          }
        }
        else {
          drupal_set_message(t('Your phone number does not match
            any existing accounts. Creation of new accounts on this site is disabled.'));
          return FALSE;
        }
      }

      $user = user_load($user->uid);
      // Checking of the status of the user is active.
      if ($user->status == 0) {
        drupal_set_message(
          t('An account has been created for you on @sitename but an administrator needs to approve your account.',
            array('@sitename' => variable_get('site_name', ''))));
      }
      // If reset password is aloowed & redirect user to reset pass page.
      elseif (empty($user->pass) && variable_get("cureti_phone_login_password_change", 1)) {
        drupal_goto(user_pass_reset_url($user));
      }
      else {
        // Login the user and redirect to the destination.
        if (cureti_phone_login_login_user($user)) {
          $destination = isset($_GET['destination']) ? $_GET['destination'] : '<front>';
          drupal_goto($destination);
        }
        else {
          drupal_set_message(t('Unable to login using Cureti Phone Login.'), 'warning');
          // Redirecting to the login page.
          drupal_goto('user');
        }
      }
    }
  }
  elseif (!isset($_GET['token'])) {
    watchdog('Cureti', 'A Cureti request code was expected but no authorization was received.');
  }

  return $error_message;
}

/**
 * Create a new user using the phone number received from Cureti.
 *
 * @param string $phone
 *   The phone number using which registration is to be done.
 *
 * @return object
 *   A newly created Drupal user account.
 */
function cureti_phone_login_create_user($phone) {
  // Creating new user.
  $edit = array(
    'name' => $phone,
    'mail' => '',
    'init' => '',
    // If user_register is "1", then no approval required.
    'status' => variable_get('user_register', 1) == 1 ? 1 : 0,
    'signature_format' => 'filtered_html',
    'timezone' => variable_get('date_default_timezone'),
    'curetilogin' => TRUE,
    // Signify this is being imported by Cureti Login.
    // So that other modules can load the account.
  );
  $account = user_save(NULL, $edit);
  return $account;
}


/**
 * Given a Drupal user object, log the user in.
 *
 * This acts as a wrapper around user_external_login() in Drupal 6 and as a full
 * replacement function in Drupal 7, since no direct equivalent exists.
 *
 * @param object $account
 *   A Drupal user account.
 *
 * @return int
 *   The uid of the user.
 */
function cureti_phone_login_login_user($account) {
  global $user;

  if ($account->status) {
    $form_state['uid'] = $account->uid;
    user_login_submit(array(), $form_state);
  }
  else {
    if ($account->access) {
      drupal_set_message(t('The account with username %name and email %mail is blocked.',
        array('%name' => $account->name, '%mail' => $account->mail)), 'error');
    }
    else {
      drupal_set_message(t('The account with username %name and email %mail is not yet activated.',
        array('%name' => $account->name, '%mail' => $account->mail)), 'error');
    }
  }

  return !empty($user->uid);
}

/**
 * Function to get the details using the access token.
 *
 * @param string $code
 *   The access_token received from the cureti server.
 *
 * @return array
 *   The json decoded data received after using the token.
 */
function cureti_phone_login_access_token($code) {
  $data = "access_token=$code";
  $merchant_id = variable_get('cureti_phone_login_merchant_id');
  $api_key = variable_get('cureti_phone_login_api_key');
  $url = "https://$merchant_id:$api_key@api.cureti.com/sms/verifyaccesstoken?authenticate";
  $options = array(
    'method' => 'POST',
    'data' => $data,
    'timeout' => 15,
    'headers' => array('Content-Type' => 'application/x-www-form-urlencoded'),
  );

  $authentication_result = drupal_http_request($url, $options);
  if ($authentication_result->code != 200) {
    $error = !empty($authentication_result->error) ? $authentication_result->error : t('(no error returned)');
    $data = !empty($authentication_result->data) ? print_r($authentication_result->data, TRUE) : t('(no data returned)');
    watchdog('Cureti Login', "Cureti Login could not fetch data from servers: <code><pre>@url</pre></code>
      Cureti's servers returned an error @error: <code><pre>@return</pre></code>",
      array('@url' => $url, '@error' => $error, '@return' => $data));
  }
  else {
    $data = json_decode($authentication_result->data);
    return $data;
  }
}
