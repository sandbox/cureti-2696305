<?php

/**
 * @file
 * Module file for Cureti One Click Login Integration.
 *
 * Cureti One Click Login module integrates the Cureti API
 * with the drupal which enables the use to login or register using their
 * phone number.
 */

define('CURETI_PHONE_LOGIN_API_BASE_URL', 'https://api.cureti.com');

/**
 * Implements hook_menu().
 */
function cureti_phone_login_menu() {
  $items = array();
  $items['admin/config/people/curetiphonelogin'] = array(
    'title' => 'Cureti One Click Phone Login',
    'description' => 'Configure Cureti One Click Phone Login settings.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('cureti_phone_login_admin_form'),
    'access arguments' => array('administer uuid'),
    'type' => MENU_NORMAL_ITEM,
    'file' => 'cureti_phone_login.admin.inc',
  );

  $items['curetilogin/connect'] = array(
    'title' => 'Cureti Phone Login',
    'page callback' => 'cureti_phone_login_action_page',
    'page arguments' => array(2),
    // Anonymous users will reach this page after phone is verified.
    'access callback' => TRUE,
    'file' => 'includes/curetilogin.connect.inc',
    'type' => MENU_CALLBACK,
  );

  return $items;
}


/**
 * Implements hook_permission().
 */
function cureti_phone_login_permission() {
  return array(
    'administer cureti_phone_login' => array(
      'title' => t('Administer Cureti Phone Login'),
      'description' => t('Allows configuration of the Cureti Login module and APIs.'),
    ),
  );
}

/**
 * Function to set properties in the login link for Cureti.
 *
 * @return array
 *   The array containing the href and merchant id.
 */
function cureti_phone_login_link_properties() {
  $return = array(
    'query' => array(
      'merchant_id' => variable_get('cureti_phone_login_merchant_id'),
    ),
    'href' => constant("CURETI_PHONE_LOGIN_API_BASE_URL") . '/login/index.php',
  );
  return $return;
}

/**
 * Function to generate login link for Cureti.
 *
 * @return string
 *   The full link which where the user will will be redirected.
 */
function cureti_phone_login_connect_link() {
  global $base_url;
  $link = cureti_phone_login_link_properties();
  $url = url($link['href'], array('query' => $link['query']));
  $redirecturl = cureti_phone_login_get_redirect_url();
  $url = $url . "&redirect_url=$redirecturl";
  $link['attributes']['class'] = isset($link['attributes']['class']) ? $link['attributes']['class'] : 'cureti-login-link';
  $link['attributes']['rel'] = 'nofollow';
  $attributes = isset($link['attributes']) ? drupal_attributes($link['attributes']) : '';
  $src = $base_url . '/' . drupal_get_path('module', 'cureti_phone_login') . '/includes/cureti-phone-login.png';
  $title = isset($link['title']) ? check_plain($link['title']) : '';
  return '<a ' . $attributes . ' href="' . $url . '"><img src="' . $src . '" alt="' . $title . '" /></a>';
}


/**
 * Function to get the redirect url parameter for Cureti Phone login.
 *
 * @return string
 *   The link with destination set if mention in the url.
 */
function cureti_phone_login_get_redirect_url() {
  // Checking if the destination is set.
  if (isset($_GET['destination'])) {
    $destination = $_GET['destination'];
    $redirecturl = url('curetilogin/connect', array(
      'absolute' => TRUE,
      'query' => array('destination' => $destination),
    ));
  }
  else {
    $redirecturl = url('curetilogin/connect', array('absolute' => TRUE));
  }
  return $redirecturl;
}

/**
 * Implements hook_form_alter().
 */
function cureti_phone_login_form_alter(&$form, &$form_state, $form_id) {
  // Adding the cureti Login button on the login and register form.
  if ($form_id == 'user_login' || $form_id == 'user_register_form') {
    $merchant_id = variable_get('cureti_phone_login_merchant_id', 0);
    $api_key = variable_get('cureti_phone_login_api_key', 0);
    if ($merchant_id && $api_key) {
      $form['cureti_login_link'] = array(
        '#prefix' => "<div class = cureti-login-link-container>",
        '#suffix' => "</div>",
        '#markup' => cureti_phone_login_connect_link(),
        '#weight' => -100,
      );
    }
  }
}
