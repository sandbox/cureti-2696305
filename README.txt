=====
Cureti One Click Phone Login
-----
Cureti One Click Phone Login was developed and is maintained by Cureti
<http://cureti.com>.


INTRODUCTION
------------

Cureti Phone Login integrates the Cureti One Click Login API
with Drupal which allows the user to login or register
in drupal using their phone number. For supported countries
and pricing please visit "http://cureti.com".
-----

REQUIREMENTS
------------

To use this module you must a merchant account on
http://cureti.com. This module has no dependies on
any other drupal module.
-----


RECOMMENDED MODULES
-------------------

You can use "Complete Profile (https://www.drupal.org/project/complete_profile)"
module if you want the users to enter more details after
registering through phone number.
-----


INSTALLATION
------------

Install as you would normally install a contributed Drupal module. See:
https://drupal.org/documentation/install/modules-themes/modules-7
for further information.
-----


CONFIGURATION
-------------

* Customize the settings in Administration » Configuration and modules »
   Cureti One Click Phone Login.

   - Enter your Merchant ID and API key and save.
     (To obtain Merchant ID and API Key, please visit http://cureti.com)
